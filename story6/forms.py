from django import forms

class PesertaForm(forms.Form):
    Peserta = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Masukkan nama',
        'type' : 'text',
        'required': True,
    }))

class AktivitasForm(forms.Form):
    Aktivitas = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Masukkan kegiatan baru',
        'type' : 'text',
        'required': True,
    })) 