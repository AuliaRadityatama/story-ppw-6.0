from django.shortcuts import render
from django.shortcuts import redirect
from .models import Participant as orang
from .models import Activity as kegiatan
from .forms import PesertaForm
from .forms import AktivitasForm

template="story6.html"

# Create your views here.
def Join1(request):
    if request.method == "POST":
        form = PesertaForm(request.POST)
        if form.is_valid():
            infoOrang = orang()
            infoOrang.Peserta = form.cleaned_data['Peserta']
            infoOrang.save()
        return redirect('/')
    else:
        infoOrang = orang.objects.all()
        form = PesertaForm()
        response = {"kuliah":infoOrang, 'form' : form}
        return render(request,template,response)

def Join2(request):
    if request.method == "POST":
        form = AktivitasForm(request.POST)
        if form.is_valid():
            infoKegiatan = kegiatan()
            infoKegiatan.Aktivitas = form.cleaned_data['Aktivitas']
            infoKegiatan.save()
        return redirect('/')
    else:
        infoKegiatan = kegiatan.objects.all()
        form = AktivitasForm()
        response = {"infoKegiatan":infoKegiatan, 'form' : form}
        return render(request,template,response)

def dynamic_lookup_view(request, my_id):
    if request.method == "POST":
        form = PesertaForm(request.POST)
        if form.is_valid():
            infoOrang = orang()
            infoOrang.Peserta = form.cleaned_data['Peserta']
            infoOrang.save()
        return redirect('/')
    else:
        obj=kegiatan.objects.get(id=my_id)
        form = PesertaForm()
        context = {"object":obj, "form": form}
        return render(request, 'daftarhadir.html', context)