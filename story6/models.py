from django.db import models

# Create your models here.
class Activity(models.Model):
    Aktivitas = models.CharField(max_length=100)
    
    def __str__(self):
        return self.Aktivitas

class Participant(models.Model):
    Peserta = models.CharField(max_length=50)
    Orang = models.ManyToManyField(Activity)
    def __str__(self):
        return self.Peserta