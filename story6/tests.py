from django.test import TestCase, Client
from django.http import HttpRequest
from .models import Participant
from .models import Activity

# Create your tests here.
class Story6(TestCase):
    def test_landing_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code,200)
    
    def test_landing_page_template(self):
        response = Client().get("")
        self.assertTemplateUsed(response,'story6.html')

    def test_models_aktivitas(self):
        Activity.objects.create(Aktivitas="Ngoding bareng")
        n = Activity.objects.all().count()
        self.assertEqual(n,1)

    def test_models_peserta(self):
        Participant.objects.create(Peserta="Pewe")
        m = Participant.objects.all().count()
        self.assertEqual(m,1)

    def test_apakah_di_halaman_formulir_ada_text_Aktivitas_dan_tombol_Kirim(self):
	    response = Client().get("")
	    html_kembalian = response.content.decode('utf8')
	    self.assertIn("Aktivitas", html_kembalian)
	    self.assertIn("Tambahkan Kegiatan", html_kembalian)

    def test_judul_tabel(self):
        response = Client().get("")
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Kegiatan", html_kembalian)
