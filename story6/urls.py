from django.urls import path
from . import views
from .views import Join1, Join2, dynamic_lookup_view

app_name = 'story6'

urlpatterns = [
    path('', Join2),
    path('<int:my_id>/', dynamic_lookup_view, name='daftar'),
]